# Link neccesary files to home directory
LINK_FILES=( bash_profile vimrc )

for file in ${LINK_FILES[@]}; do
    if [ -f "$HOME/.${file}" ]; then
        echo "File $HOME/.${file} already exists"
    else
        echo "Linking file: $HOME/.${file}";
        ln -s $HOME/.dotfiles/.${file} $HOME/.${file};
    fi
done

# Filler for setting up Git aliases
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue)<%an>%Creset' --abbrev-commit"
