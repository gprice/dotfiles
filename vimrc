" My personal .vimrc
" Super simple work in progress
syntax on
colorscheme murphy
set autoindent
set tabstop=4 shiftwidth=4 expandtab

if has("autocmd")
    " If the filetype is Makefile then we need to use tabs
    " So do not expand tabs into space.
    autocmd FileType make   set noexpandtab
endif
