# Geoff Price's dotfiles

These are just a collection of my dotfiles.

## Install

Clone to the repo to the proper location.
* `git clone git@gitlab.com:gprice/dotfiles.git $HOME/.dotfiles`

Run the install script to configure symlinks:
* `bash $HOME/.dorfiles/install.sh`
