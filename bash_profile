#  Custom Darwin (aka Mac OSx) settings
#  Helper function to compare versions
    version_gt() {
        test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"; 
    }

if [[ $(uname -s) == "Darwin" ]]; then
    if version_gt $(sw_vers -productVersion) "10.15"; then
        # Turn off Catalina Bash Deprecation Warning
        export BASH_SILENCE_DEPRECATION_WARNING=1
    fi
fi

# Set some handy options
shopt -s cdspell   # Autocorrect minor erros in `cd` command

# Some general preferences
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
export EDITOR=/usr/bin/vim

script_dir=".dotfiles"

#  Set some useful aliases
alias la='ls -A'
alias ll='ls -lah'
alias mkdir='mkdir -pv'
alias less='less -gcQ'

#  Make grep commands be in color
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

### START - Borrowed from https://github.com/mathiasbynens/dotfiles/blob/master/.aliases
#  Show active network interfaces
alias ifactive="ifconfig | pcregrep -M -o '^[^\t:]+:([^\n]|\n\t)*status: active'"

#  Flush Directory Service (DNS) cache
alias flush="dscacheutil -flushcache && killall -HUP mDNSResponder"

#  Canonical hex dump; some systems have this symlinked
command -v hd > /dev/null || alias hd="hexdump -C"

#  macOS has no `md5sum`, so use `md5` as a fallback
command -v md5sum > /dev/null || alias md5sum="md5"

#  macOS has no `sha1sum`, so use `shasum` as a fallback
command -v sha1sum > /dev/null || alias sha1sum="shasum"

#  Hide/show all desktop icons (useful when presenting)
alias hidedesktop="defaults write com.apple.finder CreateDesktop -bool false && killall Finder"
alias showdesktop="defaults write com.apple.finder CreateDesktop -bool true && killall Finder"

#  One of @janmoesen’s ProTip™s
for method in GET HEAD POST PUT DELETE TRACE OPTIONS; do
	alias "${method}"="lwp-request -m '${method}'"
done

#  Stuff I never really use but cannot delete either because of http://xkcd.com/530/
alias stfu="osascript -e 'set volume output muted true'"
alias pumpitup="osascript -e 'set volume output volume 100'"

#  Print each PATH entry on a separate line
alias path='echo -e ${PATH//:/\\n}'
### END

#  Enable autocompletion for what we can.
command -v aws > /dev/null && complete -C aws_completer aws
command -v terraform > /dev/null && complete -C terraform terraform 

#  Export a local bin directory for personal executables
export PATH="${HOME}/bin:${PATH}"

#   rmssh: Remove known host from local ssh config
#   ---------------------------------------------------------
    rmssh () {
        sed -i "" "/^${1}.*/d" ${HOME}/.ssh/known_hosts
    }

#   rmdocker: Stop and remove a running docker container
#   ---------------------------------------------------------
    rmdocker () {
        docker stop ${1} && docker rm ${1}
    }

#   mcd: Make a directory and change into that directory
#   ---------------------------------------------------------
    mcd () { mkdir -p "$1" && cd "$1"; } 

#   extract:  Extract most know archives with one command
#   ---------------------------------------------------------
    extract () {
        if [ -f $1 ] ; then
          case $1 in
            *.tar.bz2)   tar xjf $1     ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1     ;;
            *.rar)       unrar e $1     ;;
            *.gz)        gunzip $1      ;;
            *.tar)       tar xf $1      ;;
            *.tbz2)      tar xjf $1     ;;
            *.tgz)       tar xzf $1     ;;
            *.zip)       unzip $1       ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1        ;;
            *)     echo "'$1' cannot be extracted via extract()" ;;
             esac
         else
             echo "'$1' is not a valid file"
         fi
    }

#   contains:  Bash function to see if an array contains a value
#   Returns both a string an appropriate return code
#     example:
#       $ A=(one two three)
#       $ contains ${A[@]} one # returns true
#       $ contains ${A[@]} four # returns false
#   ---------------------------------------------------------
    contains () {
        local n=$#
        local value=${!n}
        for ((i=1;i < $#;i++)) {
            if [ "${!i}" == "${value}" ]; then
                echo "true"
                return 0
            fi
        }
        echo "false"
        return 1
    }

#   spotlight: Search for a file using MacOS Spotlight's metadata
#   -----------------------------------------------------------
    spotlight () { mdfind "kMDItemDisplayName == '$@'wc"; }

#  Import some other local profile files if the exist
#  - Keep your outside this repo if you desire. I keep mine in a private snippet in gitlab
if [[ -f "${HOME}/${script_dir}/custom" ]]; then
    source "${HOME}/${script_dir}/custom";
fi

#  Handle some Git bits
if [[ -d "/usr/local/etc/bash_completion.d" ]]; then
    source /usr/local/etc/bash_completion.d/git-completion.bash
    source /usr/local/etc/bash_completion.d/git-prompt.sh 
fi

#  If AWS prompt file exists, import it, otherwise set default for prompt
if [[ -f "${HOME}/${script_dir}/aws" ]]; then
    source "${HOME}/${script_dir}/aws";
else
    __aws_account () { echo ""; }
fi

#  Set a minimalistic prompt. Update if you desire a change 😊
#  - Must install git colors: https://gist.githubusercontent.com/vratiu/9780109/
if [[ -f "${HOME}/${script_dir}/bash_colors.sh" && $(type __git_ps1 &> /dev/null; echo $?) == "0" ]]; then
    source "${HOME}/${script_dir}/bash_colors.sh"
    export PS1=$Color_Off$PathFull$Color_Off'$(git branch &>/dev/null;\
    if [ $? -eq 0 ]; then \
    echo "$(echo `git status` | grep "nothing to commit" > /dev/null 2>&1; \
    if [ "$?" -eq "0" ]; then \
        # @4 - Clean repository - nothing to commit
        echo "'$Green'"$(__git_ps1 " (%s)"); \
    else \
        # @5 - Changes to working tree
        echo "'$IRed'"$(__git_ps1 " (%s)"); \
    fi) '$BYellow$Color_Off$(__aws_account)'\$ "; \
    else \
    # @2 - Prompt when not in GIT repo
    echo "'$White$Color_Off$(__aws_account)'\$ "; \
    fi)'
fi
